﻿namespace Otus.Solid.GuessTheNumber.Options
{
    public class GuessGameOptions
    {
        public const string GuessGame = "GuessGame";
        public int Attempts { get; set; }
        public int MinValue { get; set; }
        public int MaxValue { get; set; }
        public string NumberGeneratorName { get; set; } = string.Empty;
    }
}
