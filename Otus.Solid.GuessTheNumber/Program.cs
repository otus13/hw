﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Otus.Solid.GuessTheNumber.GuessGame;
using Otus.Solid.GuessTheNumber.GuessGame.GameData;
using Otus.Solid.GuessTheNumber.GuessGame.Services;
using Otus.Solid.GuessTheNumber.Options;
using System.IO;

namespace Otus.Solid.GuessTheNumber
{
    class Program
    {
        static void Main(string[] args)
        {
            var host = Host.CreateDefaultBuilder()
                .ConfigureServices((context, services) =>
                {
                    var configurationBuilder = new ConfigurationBuilder()
                        .SetBasePath(Directory.GetCurrentDirectory())
                        .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

                    var configuration = configurationBuilder.Build();

                    services
                        .AddSingleton<GameDataContext, GameDataContext>()
                        .AddTransient<IGame, Game>()
                        .AddTransient<IGameService, GameService>()
                        .AddOptions<GuessGameOptions>()
                            .Bind(configuration.GetSection(GuessGameOptions.GuessGame));

                }).Build();

            var game = ActivatorUtilities.GetServiceOrCreateInstance<IGame>(host.Services);

            game.Start();
        }
    }
}
