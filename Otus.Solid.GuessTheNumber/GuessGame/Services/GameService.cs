﻿using Otus.Solid.GuessTheNumber.GuessGame.GameData;
using Otus.Solid.GuessTheNumber.GuessGame.NumberGenerators;
using Otus.Solid.GuessTheNumber.GuessGame.Statements;

namespace Otus.Solid.GuessTheNumber.GuessGame.Services
{
    public class GameService : IGameService
    {
        private readonly GameDataContext _gameData;

        public GameService(GameDataContext gameData)
        {
            _gameData = gameData;
        }

        public void SetNumber(INumberGenerator numberGenerator)
        {
            var number = numberGenerator.GetNumber(_gameData.MinValue, _gameData.MaxValue);
            _gameData.SetNumber(number);
        }

        public Statement GetGameStarterStatement()
        {
            return new GameStarterStatement(_gameData.MinValue, _gameData.MaxValue, _gameData.AttemptsCount);
        }

        public Statement GetStatement(int answerNumber)
        {
            _gameData.DecreaseAttempt();

            if (_gameData.Number == answerNumber)
                return new ResultCorrectStatement(_gameData.AttemptsCount, _gameData.AttemptsLeftCount);

            if (_gameData.AttemptsLeftCount == 0)
                return new AttemptsAreOverStatement();

            return new ResultIncorrectStatement(_gameData.Number > answerNumber, _gameData.AttemptsLeftCount);
        }
    }
}
