﻿using Otus.Solid.GuessTheNumber.GuessGame.NumberGenerators;
using Otus.Solid.GuessTheNumber.GuessGame.Statements;

namespace Otus.Solid.GuessTheNumber.GuessGame.Services
{
    public interface IGameService
    {
        void SetNumber(INumberGenerator numberGenerator);
        Statement GetGameStarterStatement();
        Statement GetStatement(int answerNumber);
    }
}