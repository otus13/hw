﻿using Microsoft.Extensions.Options;
using Otus.Solid.GuessTheNumber.GuessGame.NumberGenerators;
using Otus.Solid.GuessTheNumber.GuessGame.Services;
using Otus.Solid.GuessTheNumber.Options;
using System;

namespace Otus.Solid.GuessTheNumber.GuessGame
{
    public class Game : IGame
    {
        private readonly INumberGenerator _numberGenerator;
        private readonly IGameService _gameService;

        public Game(IOptions<GuessGameOptions> options, IGameService gameService)
        {
            _gameService = gameService;
            _numberGenerator = NumberGeneratorFactory.GetByName(options.Value.NumberGeneratorName);
        }

        public void Start()
        {
            _gameService.SetNumber(_numberGenerator);

            var nextStatement = _gameService.GetGameStarterStatement();

            while (!nextStatement.IsGameOver)
            {
                Console.WriteLine(nextStatement.OutputText);

                var inputNumber = GetInputNumber();

                nextStatement = _gameService.GetStatement(inputNumber);
            }
            Console.WriteLine(nextStatement.OutputText);
            Console.WriteLine("\r\nSee you next time!");
        }

        private static int GetInputNumber()
        {
            bool isCorrectInput;
            int value;
            do
            {
                var text = Console.ReadLine();
                isCorrectInput = int.TryParse(text, out value);

                if (!isCorrectInput)
                    Console.WriteLine($"Incorrect format. '{value}' is not a number");

            } while (!isCorrectInput);

            return value;
        }
    }
}
