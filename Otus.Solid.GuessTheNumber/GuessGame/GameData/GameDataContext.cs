﻿using Microsoft.Extensions.Options;
using Otus.Solid.GuessTheNumber.Options;

namespace Otus.Solid.GuessTheNumber.GuessGame.GameData
{
    public class GameDataContext
    {
        public int MinValue { get; }
        public int MaxValue { get; }
        public int AttemptsCount { get; }
        public int AttemptsLeftCount { get; private set; }
        public int Number { get; private set; }

        public GameDataContext(IOptions<GuessGameOptions> options)
        {
            AttemptsCount = options.Value.Attempts;
            AttemptsLeftCount = options.Value.Attempts;
            MinValue = options.Value.MinValue;
            MaxValue = options.Value.MaxValue;
        }

        public void SetNumber(int number)
        {
            Number = number;
        }

        public void DecreaseAttempt()
        {
            AttemptsLeftCount--;
        }
    }
}
