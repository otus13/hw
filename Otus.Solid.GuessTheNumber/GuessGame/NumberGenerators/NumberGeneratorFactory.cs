﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace Otus.Solid.GuessTheNumber.GuessGame.NumberGenerators
{
    public class NumberGeneratorFactory
    {
        private static readonly IReadOnlyDictionary<string, INumberGenerator> NumberGenerators;

        static NumberGeneratorFactory()
        {
            var numberGeneratorType = typeof(INumberGenerator);
            NumberGenerators = numberGeneratorType.Assembly.ExportedTypes
                .Where(a => numberGeneratorType.IsAssignableFrom(a) && !a.IsAbstract && !a.IsInterface)
                .Select(Activator.CreateInstance)
                .Cast<INumberGenerator>()
                .ToImmutableDictionary(a => a.GetType().Name, a => a);
        }

        public static INumberGenerator GetByName(string name)
        {
            return NumberGenerators.ContainsKey(name)
                ? NumberGenerators[name]
                : NumberGenerators[nameof(DefaultNumberGenerator)];
        }
    }
}
