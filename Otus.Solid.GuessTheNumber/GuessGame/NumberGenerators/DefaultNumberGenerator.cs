﻿using System;

namespace Otus.Solid.GuessTheNumber.GuessGame.NumberGenerators
{
    public class DefaultNumberGenerator : INumberGenerator
    {
        private readonly Random _random = new();

        public int GetNumber(int minValue, int maxValue)
        {
            return _random.Next(minValue, maxValue);
        }
    }
}
