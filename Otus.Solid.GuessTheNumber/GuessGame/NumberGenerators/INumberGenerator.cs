﻿namespace Otus.Solid.GuessTheNumber.GuessGame.NumberGenerators
{
    public interface INumberGenerator
    {
        public int GetNumber(int minValue, int maxValue);
    }
}
