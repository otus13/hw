﻿namespace Otus.Solid.GuessTheNumber.GuessGame.Statements
{
    public class ResultCorrectStatement : Statement
    {
        public override bool IsGameOver { get; } = true;
        public override string OutputText { get; }

        public ResultCorrectStatement(int attemptsCount, int attemptsLeftCount)
        {
            OutputText = $"You won! Result is correct. You have used {attemptsCount - attemptsLeftCount} from {attemptsCount} attempts.";
        }
    }
}
