﻿namespace Otus.Solid.GuessTheNumber.GuessGame.Statements
{
    public class ResultIncorrectStatement : Statement
    {
        public override bool IsGameOver { get; } = false;
        public override string OutputText { get; }

        public ResultIncorrectStatement(bool isMore, int attemptsLeftCount)
        {
            var tip = isMore ? "more" : "less";
            OutputText = $"The number is {tip} than the input. You have {attemptsLeftCount} left";
        }
    }
}
