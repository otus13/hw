﻿namespace Otus.Solid.GuessTheNumber.GuessGame.Statements
{
    public class GameStarterStatement : Statement
    {
        public override bool IsGameOver { get; } = false;
        public override string OutputText { get; }

        public GameStarterStatement(int minValue, int maxValue, int attemptsLeftCount)
        {
            OutputText = $"Guess number from {minValue} to {maxValue} including. You have {attemptsLeftCount} attempts.";
        }
    }
}
