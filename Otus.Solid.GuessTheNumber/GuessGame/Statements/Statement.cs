﻿namespace Otus.Solid.GuessTheNumber.GuessGame.Statements
{
    public abstract class Statement
    {
        public abstract bool IsGameOver { get; }
        public abstract string OutputText { get; }
    }
}
