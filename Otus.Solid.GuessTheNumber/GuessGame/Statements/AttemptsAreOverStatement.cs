﻿namespace Otus.Solid.GuessTheNumber.GuessGame.Statements
{
    public class AttemptsAreOverStatement : Statement
    {
        public override bool IsGameOver { get; } = true;
        public override string OutputText { get; }

        public AttemptsAreOverStatement()
        {
            OutputText = $"Game over. You lose. There is no more attempts left";
        }
    }
}
