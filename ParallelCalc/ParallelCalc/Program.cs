﻿using ParallelCalc.ArraySumCalculator;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ParallelCalc
{
    class Program
    {
        static readonly Random Rand = new Random();

        static void Main(string[] args)
        {
            var elementCounts = new int[]
            {
                100_000,
                1_000_000,
                10_000_000
            };

            foreach (var count in elementCounts)
            {
                Console.WriteLine($"\r\nRun calculation for {count} array.\r\n");

                var array = GenerateIntArray(count);

                var calcSumWays = new List<ArraySumCalc>
                {
                    new RegularCalc(array),
                    new ParallelLinqCalc(array),
                    new MultiThreadedCalc(array, 2),
                    new MultiThreadedCalc(array, Environment.ProcessorCount),
                };

                calcSumWays.ForEach(calcSumWay =>
                {
                    calcSumWay.Run();
                    WriteResult(calcSumWay);
                });
            }

            Console.WriteLine("Calculations finished.");
        }

        static int[] GenerateIntArray(int uBound)
        {
            return new int[uBound].Select((a) => Rand.Next(0, int.MaxValue)).ToArray();
        }

        static void WriteResult(ArraySumCalc sumCalc)
        {
            Console.WriteLine($"{sumCalc.Name} execution");
            Console.WriteLine($"Result: {sumCalc.Result}. Execution time: {sumCalc.ElapsedMilliseconds} mc.\r\n");
        }
    }
}