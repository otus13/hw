﻿using System;
using System.Linq;
using System.Threading;

namespace ParallelCalc.ArraySumCalculator
{
    public class MultiThreadedCalc : ArraySumCalc
    {
        public override string Name { get; }
        private ThreadArrayItem[] ThreadArrayItem { get; }
        private readonly object _lock = new object();
        private long _sharedResult;

        public MultiThreadedCalc(int[] array, int threadCount)
            : base(array)
        {
            Name = $"Multi Threaded with {threadCount} threads";
            ThreadArrayItem = new ThreadArrayItem[threadCount];

            var chunkLength = (int)Math.Ceiling((double)CalcArray.Length / threadCount);

            for (var i = 0; i < threadCount; i++)
            {
                var chunkArray = CalcArray.Skip(i * chunkLength).Take(chunkLength).ToArray();

                ThreadArrayItem[i] = new ThreadArrayItem(chunkArray, CalcSum);

            }
        }

        private void CalcSum(int[] array)
        {
            long sum = 0;

            foreach (var value in array)
            {
                sum += value;
            }

            lock (_lock)
            {
                _sharedResult += sum;
            }
        }

        protected override long CalcSum()
        {
            _sharedResult = 0;

            for (var i = 0; i < ThreadArrayItem.Length; i++)
            {
                var index = i;
                ThreadArrayItem[index].Thread.Start();
            }

            WaitHandle.WaitAll(ThreadArrayItem.Select(t => t.Handle).ToArray());

            return _sharedResult;
        }
    }
}