﻿using System.Linq;
using System.Threading;

namespace ParallelCalc.ArraySumCalculator
{
    public class ParallelLinqCalc : ArraySumCalc
    {
        public override string Name { get; } = "Parallel Linq";

        public ParallelLinqCalc(int[] array)
            : base(array)
        {
        }

        protected override long CalcSum()
        {
            return CalcArray.AsParallel().Sum(x => (long)x);
        }
    }
}