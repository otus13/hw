﻿using System;
using System.Threading;

namespace ParallelCalc.ArraySumCalculator
{
    public class ThreadArrayItem
    {
        public Thread Thread { get; }
        public WaitHandle Handle { get; }

        public ThreadArrayItem(int[] array, Action<int[]> action)
        {
            var handle = new AutoResetEvent(false);
            Handle = handle;
            Thread = new Thread(() =>
            {
                action(array);
                handle.Set();
            });
        }
    }
}