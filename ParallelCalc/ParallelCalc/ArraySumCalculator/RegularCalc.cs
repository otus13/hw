﻿namespace ParallelCalc.ArraySumCalculator
{
    public class RegularCalc : ArraySumCalc
    {
        public override string Name { get; } = "Regular foreach";

        public RegularCalc(int[] array)
            : base(array)
        {
        }

        protected override long CalcSum()
        {
            long sum = 0;

            foreach (var value in CalcArray)
            {
                sum += value;
            }

            return sum;
        }
    }
}