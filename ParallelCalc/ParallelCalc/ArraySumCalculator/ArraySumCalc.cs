﻿using System.Diagnostics;

namespace ParallelCalc.ArraySumCalculator
{
    public abstract class ArraySumCalc
    {
        private readonly Stopwatch _timer;

        protected int[] CalcArray { get; }
        public long Result { get; private set; }

        public abstract string Name { get; }

        public long ElapsedMilliseconds => _timer.ElapsedMilliseconds;

        protected ArraySumCalc(int[] array)
        {
            _timer = new Stopwatch();
            CalcArray = array;
        }

        public void Run()
        {
            _timer.Restart();

            Result = CalcSum();

            _timer.Stop();
        }

        protected abstract long CalcSum();
    }
}
