﻿using System;
using System.Collections.Generic;

namespace Teaching.Events.Document.Receiver
{
    public class DocumentEventArgs : EventArgs
    {
        public Dictionary<string, bool> FilesToReceive { get; init; }
    }
}
