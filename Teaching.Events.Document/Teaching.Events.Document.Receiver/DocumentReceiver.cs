﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Timers;

namespace Teaching.Events.Document.Receiver
{
    public delegate void DocumentsReady(object sender, DocumentEventArgs args);

    public delegate void TimedOut(object sender, DocumentEventArgs args);

    public class DocumentReceiver : IDisposable
    {
        private readonly Dictionary<string, bool> _filesToReceive;
        private readonly FileSystemWatcher _fileWatcher;
        private readonly Timer _timer;

        public event DocumentsReady DocumentsReady;
        public event TimedOut TimedOut;

        private bool IsDisposed { get; set; }

        public DocumentReceiver(params string[] fileNames)
        {
            _filesToReceive = fileNames.ToDictionary(f => f, f => false);
            _fileWatcher = new FileSystemWatcher();
            _timer = new Timer();
        }

        public void Start(string targetDirectory, int waitingSeconds)
        {
            if (IsDisposed) throw new ObjectDisposedException("DocumentReceiver is disposed");

            _fileWatcher.Path = targetDirectory;
            _fileWatcher.Created -= CheckFileCreated;
            _fileWatcher.Created += CheckFileCreated;
            _fileWatcher.EnableRaisingEvents = true;

            _timer.Elapsed -= OnTimedOut;
            _timer.Elapsed += OnTimedOut;
            _timer.Interval = waitingSeconds * 1000;
            _timer.Start();
        }

        private void CheckFileCreated(object sender, FileSystemEventArgs e)
        {
            if (e.Name == null || !_filesToReceive.ContainsKey(e.Name)) return;

            _filesToReceive[e.Name] = true;

            if (!_filesToReceive.ContainsValue(false))
            {
                OnDocumentsReady();
            }
        }

        private void OnDocumentsReady()
        {
            Unsubscribe();

            var eArgs = new DocumentEventArgs { FilesToReceive = _filesToReceive };

            DocumentsReady?.Invoke(this, eArgs);
        }

        private void OnTimedOut(object sender, EventArgs args)
        {
            Unsubscribe();

            var eArgs = new DocumentEventArgs { FilesToReceive = _filesToReceive };

            TimedOut?.Invoke(this, eArgs);
        }

        private void Unsubscribe()
        {
            if (_fileWatcher != null) _fileWatcher.Created -= CheckFileCreated;
            if (_timer != null) _timer.Elapsed -= OnTimedOut;
        }

        public void Dispose()
        {
            if (IsDisposed) return;

            Unsubscribe();

            _fileWatcher?.Dispose();
            _timer?.Dispose();

            IsDisposed = true;
        }
    }
}