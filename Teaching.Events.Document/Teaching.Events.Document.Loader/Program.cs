﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using Teaching.Events.Document.Receiver;

namespace Teaching.Events.Document.Loader
{
    class Program
    {
        static void Main(string[] args)
        {
            var filePath = AppDomain.CurrentDomain.BaseDirectory;
            var fileNames = new[] { "Паспорт.jpg", "Заявление.txt", "Фото.jpg" };
            var waitingSeconds = 20;

            File.Delete(Path.Combine(filePath, fileNames[0]));
            File.Delete(Path.Combine(filePath, fileNames[1]));
            File.Delete(Path.Combine(filePath, fileNames[2]));

            var documentReceiver = new DocumentReceiver(filePath, fileNames);
            documentReceiver.DocumentsReady += Loader_DocumentReady;
            documentReceiver.TimedOut += Loader_TimedOut;

            Console.WriteLine("Receiving documents...");

            documentReceiver.Start(waitingSeconds);

            File.Create(Path.Combine(filePath, fileNames[0])).Close();
            File.Create(Path.Combine(filePath, fileNames[1])).Close(); ;
            File.Create(Path.Combine(filePath, fileNames[2])).Close(); ;

            Console.ReadKey();

            File.Delete(Path.Combine(filePath, fileNames[0]));
            File.Delete(Path.Combine(filePath, fileNames[1]));
            File.Delete(Path.Combine(filePath, fileNames[2]));

            documentReceiver.Start(waitingSeconds);

            File.Create(Path.Combine(filePath, fileNames[0])).Close();
            File.Create(Path.Combine(filePath, fileNames[1])).Close(); ;
            File.Create(Path.Combine(filePath, fileNames[2])).Close(); ;

            Console.ReadKey();
        }

        private static void Loader_TimedOut(object sender, DocumentEventArgs args)
        {
            var fileNames = args.FilesToReceive.ToList()
                .Where(f => !f.Value)
                .Select(f => f.Key)
                .Aggregate((f, s) => f + ", " + s);

            System.Console.WriteLine($"Failed to receive following documents during timeout: {fileNames}");
        }

        private static void Loader_DocumentReady(object sender, DocumentEventArgs args)
        {
            var fileNames = args.FilesToReceive.ToList()
                .Select(f => f.Key)
                .Aggregate((f, s) => f + ", " + s);

            System.Console.WriteLine($"Your documents received: {fileNames}");
        }
    }
}
