﻿using ConsoleEF.Models.Types;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ConsoleEF.Models
{
    public class Transaction
    {
        [Key]
        public int Id { get; set; }

        public DateTime Time { get; set; }
        
        public TransactionType Type { get; set; }

        [Column(TypeName = "money")]
        public decimal Amount { get; set; }

        [Required]
        [ForeignKey("Account")]
        public int AccountId { get; set; }
    }
}
