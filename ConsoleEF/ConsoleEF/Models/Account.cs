﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ConsoleEF.Models
{
    public class Account
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(20)]
        public string Number { get; set; }

        public DateTime ExpiredDate { get; set; }

        [Required]
        [ForeignKey("Person")]
        public int PersonId { get; set; }

        public virtual ICollection<Transaction> Transactions { get; set; }
    }
}
