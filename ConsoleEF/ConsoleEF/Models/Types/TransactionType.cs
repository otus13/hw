﻿namespace ConsoleEF.Models.Types
{
    public enum TransactionType
    {
        None = 0,
        Deposit = 1,
        Withdrawal = 2
    }
}
