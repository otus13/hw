﻿using ConsoleEF.Data;

namespace ConsoleEF.Seed
{
    public static class SeedHelper
    {
        public static void SeedHostDb(DataContext context)
        {
            new DefaultModelCreator(context).Create();
        }
    }
}
