﻿using ConsoleEF.Data;
using ConsoleEF.Models;
using ConsoleEF.Models.Types;
using System;
using System.Linq;

namespace ConsoleEF.Seed
{
    public class DefaultModelCreator
    {
        private readonly DataContext context;

        public DefaultModelCreator(DataContext context)
        {
            this.context = context;
        }

        public void Create()
        {
            if (context.Person.Any())
            {
                return;
            }

            var persons = new Person[]
            {
                new Person {FirstName = "Ivan", LastName = "Ivanov", BirthDate = DateTime.Parse("1998-09-01")},
                new Person {FirstName = "Petr", LastName = "Petrov", BirthDate = DateTime.Parse("1999-09-01")},
                new Person {FirstName = "Sergei", LastName = "Sidorov", BirthDate = DateTime.Parse("2000-09-01")},
                new Person {FirstName = "Aleksey", LastName = "Alekseev", BirthDate = DateTime.Parse("2001-09-01")},
                new Person {FirstName = "Anton", LastName = "Antonov", BirthDate = DateTime.Parse("2002-09-01")}
            };
            context.Person.AddRange(persons);
            context.SaveChanges();

            var accounts = new Account[]
            {
                new Account {PersonId = persons[0].Id, Number = "111111111", ExpiredDate = DateTime.Parse("2023-09-01")},
                new Account {PersonId = persons[0].Id, Number = "222222222", ExpiredDate = DateTime.Parse("2022-09-01")},
                new Account {PersonId = persons[1].Id, Number = "333333333", ExpiredDate = DateTime.Parse("2021-09-01")},
                new Account {PersonId = persons[2].Id, Number = "444444444", ExpiredDate = DateTime.Parse("2023-02-01")},
                new Account {PersonId = persons[3].Id, Number = "555555555", ExpiredDate = DateTime.Parse("2023-03-01")},
            };
            context.Account.AddRange(accounts);
            context.SaveChanges();

            var transactions = new Transaction[]
           {
                new Transaction {AccountId = accounts[0].Id, Time = DateTime.Parse("2020-01-01 12:12:10"), Type = TransactionType.Deposit, Amount = 100000},
                new Transaction {AccountId = accounts[0].Id, Time = DateTime.Parse("2020-01-02 12:12:11"), Type = TransactionType.Withdrawal, Amount = 500},
                new Transaction {AccountId = accounts[0].Id, Time = DateTime.Parse("2020-01-03 12:12:12"), Type = TransactionType.Withdrawal, Amount = 700},
                new Transaction {AccountId = accounts[1].Id, Time = DateTime.Parse("2020-01-04 12:12:13"), Type = TransactionType.Deposit, Amount = 200000},
                new Transaction {AccountId = accounts[1].Id, Time = DateTime.Parse("2020-01-05 12:12:14"), Type = TransactionType.Withdrawal, Amount = 800},
           };
            context.Transaction.AddRange(transactions);
            context.SaveChanges();
        }
    }
}
