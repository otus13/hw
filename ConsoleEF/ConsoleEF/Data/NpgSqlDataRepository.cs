﻿using System.Linq;

namespace ConsoleEF.Data
{
    public class NpgSqlDataRepository<TEntity> : IDataRepository<TEntity> where TEntity : class
    {
        private readonly DataContext dataContext;

        public NpgSqlDataRepository(DataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public IQueryable<TEntity> GetAll()
        {
            return dataContext.Set<TEntity>();
        }

        public void Create(TEntity entity)
        {
            dataContext.Add(entity);
            dataContext.SaveChanges();
        }
    }
}
