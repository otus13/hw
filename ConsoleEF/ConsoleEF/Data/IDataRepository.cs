﻿using System.Linq;

namespace ConsoleEF.Data
{
    public interface IDataRepository<TEntity> where TEntity : class
    {
        void Create(TEntity entity);
        IQueryable<TEntity> GetAll();
    }
}
