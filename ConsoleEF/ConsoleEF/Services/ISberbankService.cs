﻿using ConsoleEF.Models;
using ConsoleEF.Models.Types;
using System.Collections.Generic;

namespace ConsoleEF.Services
{
    public interface ISberbankService
    {
        IEnumerable<Person> OutputAllPersonWithTransactions();

        void AddTransaction(string accountNumber, TransactionType type, decimal amount);
    }
}