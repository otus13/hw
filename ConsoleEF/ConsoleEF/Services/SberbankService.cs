﻿using ConsoleEF.Data;
using ConsoleEF.Models;
using ConsoleEF.Models.Types;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleEF.Services
{
    public class SberbankService : ISberbankService
    {
        private readonly IDataRepository<Person> personRepository;
        private readonly IDataRepository<Account> accountRepository;
        private readonly IDataRepository<Transaction> transactionRepository;

        public SberbankService(IDataRepository<Person> personRepository
            , IDataRepository<Account> accountRepository
            , IDataRepository<Transaction> transactionRepository)
        {
            this.personRepository = personRepository;
            this.accountRepository = accountRepository;
            this.transactionRepository = transactionRepository;
        }

        public IEnumerable<Person> OutputAllPersonWithTransactions()
        {
            var result = personRepository.GetAll()
                .Include(p => p.Accounts)
                .ThenInclude(a => a.Transactions)
                .ToList();

            return result;
        }

        public void AddTransaction(string accountNumber, TransactionType type, decimal amount)
        {
            var account = accountRepository.GetAll().First(a => a.Number == accountNumber);

            var transaction = new Transaction
            {
                AccountId = account.Id,
                Type = type,
                Amount = amount,
                Time = DateTime.Now
            };

            transactionRepository.Create(transaction);
        }
    }

}
