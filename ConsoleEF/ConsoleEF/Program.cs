﻿using ConsoleEF.Data;
using ConsoleEF.Models.Types;
using ConsoleEF.Seed;
using ConsoleEF.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Text;
using System.Text.Json;

namespace ConsoleEF
{
    partial class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;

            var host = Host.CreateDefaultBuilder()
                .ConfigureServices((context, services) =>
                {
                    services.AddTransient(typeof(IDataRepository<>), typeof(NpgSqlDataRepository<>));
                    services.AddDbContext<DataContext>(ServiceLifetime.Transient);
                }).Build();

            var dataContext = ActivatorUtilities.CreateInstance<DataContext>(host.Services);
            SeedHelper.SeedHostDb(dataContext);

            var sberService = ActivatorUtilities.CreateInstance<SberbankService>(host.Services);

            var personWithTransactions = sberService.OutputAllPersonWithTransactions();

            foreach (var personTransaction in personWithTransactions)
            {
                var jsonString = JsonSerializer.Serialize(personTransaction, new() { WriteIndented = true });

                Console.WriteLine($" Person transactions: {jsonString}");
                Console.WriteLine();
            }

            var accountNumber = "111111111";

            sberService.AddTransaction(accountNumber, TransactionType.Withdrawal, 1500);
        }
    }
}
