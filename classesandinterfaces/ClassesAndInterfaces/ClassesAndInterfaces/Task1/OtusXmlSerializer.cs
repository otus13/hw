﻿using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;
using System.IO;
using System.Xml;

namespace ClassesAndInterfaces.Task1
{
    public class OtusXmlSerializer<T> : ISerializer<T>
    {
        private static readonly XmlWriterSettings XmlWriterSettings;
        private static readonly IExtendedXmlSerializer Serializer;

        static OtusXmlSerializer()
        {
            XmlWriterSettings = new XmlWriterSettings { Indent = true };
            Serializer = new ConfigurationContainer()
                .UseAutoFormatting()
                .Type<T>()
                .Create();
        }

        public string Serialize<T>(T item)
        {
            return Serializer.Serialize(XmlWriterSettings, item);
        }

        public T Deserialize<T>(Stream stream)
        {
            return Serializer.Deserialize<T>(stream);
        }
    }
}
