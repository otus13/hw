﻿using System.IO;

namespace ClassesAndInterfaces.Task1
{
    public interface ISerializer<T>
    {
        T Deserialize<T>(Stream stream);

        string Serialize<T>(T items);
    }
}
