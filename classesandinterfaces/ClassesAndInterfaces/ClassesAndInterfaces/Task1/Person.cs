﻿using System;

namespace ClassesAndInterfaces.Task1
{
    public class Person : IComparable
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public short Age { get; set; }

        public int CompareTo(object obj)
        {
            var person = obj as Person;
            if (person == null)
            {
                throw new ArgumentException($"Argument is not a Person type");
            }

            if (this.Age == person.Age)
            {
                return 0;
            }
            else if (this.Age > person.Age)
            {
                return 1;
            }

            return -1;
        }
    }
}
