﻿namespace ClassesAndInterfaces.Task1
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;

    public class OtusStreamReader<T> : IEnumerable<T>, IDisposable
    {
        private readonly MemoryStream stream;
        private readonly ISerializer<T> serializer;
        private bool isDisposed = false;

        public OtusStreamReader(Stream stream, ISerializer<T> serializer)
        {
            var uniEncoding = new UTF8Encoding();
            var streamReader = new StreamReader(stream, Encoding.UTF8);
            var bytes = uniEncoding.GetBytes(streamReader.ReadToEnd());

            this.stream = new MemoryStream(bytes);
            this.serializer = serializer;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        public IEnumerator<T> GetEnumerator()
        {
            var items = serializer.Deserialize<T[]>(stream);

            for (var i = 0; i < items.Length; ++i)
            {
                yield return items[i];
            }
        }

        public void Dispose()
        {
            Dispose(isDisposed);
        }

        private void Dispose(bool disposed)
        {
            if (!disposed)
            {
                stream.Dispose();
                isDisposed = true;
            }
        }
    }
}
