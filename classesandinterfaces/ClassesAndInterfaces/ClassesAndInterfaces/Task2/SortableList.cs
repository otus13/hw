﻿using System;
using System.Collections.Generic;

namespace ClassesAndInterfaces.Task2
{
    public class SortableList<T> : List<T>, IAlgorithm<SortableList<T>>
        where T : IComparable
    {
        public new void Sort()
        {
            HoareSort(this, 0, this.Count - 1);
        }

        private static void HoareSort(List<T> list, int start, int end)
        {
            if (end == start)
            {
                return;
            }

            var pivot = list[end];
            var storeIndex = start;
            for (int i = start; i <= end - 1; i++)
            {
                if (list[i].CompareTo(pivot) < 0)
                {
                    var t = list[i];
                    list[i] = list[storeIndex];
                    list[storeIndex] = t;
                    storeIndex++;
                }
            }

            var n = list[storeIndex];
            list[storeIndex] = list[end];
            list[end] = n;
            if (storeIndex > start)
            {
                HoareSort(list, start, storeIndex - 1);
            }

            if (storeIndex < end)
            {
                HoareSort(list, storeIndex + 1, end);
            }
        }
    }
}
