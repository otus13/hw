﻿using System.Collections;

namespace ClassesAndInterfaces.Task2
{
    public interface IAlgorithm<T>
        where T : IEnumerable
    {
        void Sort();
    }
}
