﻿using System;

namespace ClassesAndInterfaces.Task3
{
    public class AccountService : IAccountService
    {
        private readonly IRepository<Account> reporsitory;

        public AccountService(IRepository<Account> reporsitory)
        {
            this.reporsitory = reporsitory;
        }

        public void AddAccount(Account account)
        {
            if (string.IsNullOrWhiteSpace(account.FirstName))
            {
                throw new ArgumentNullException($"{nameof(account.FirstName)} cannot be empty");
            }

            if (string.IsNullOrWhiteSpace(account.LastName))
            {
                throw new ArgumentNullException($"{nameof(account.LastName)} cannot be empty");
            }

            if (GetAge(account.BirthDate) < 18)
            {
                throw new ArgumentException($"{nameof(account.BirthDate)} is incorrect. Age cannot be less than 18 years");
            }

            reporsitory.Add(account);
        }

        private static int GetAge(DateTime datetime)
        {
            return new DateTime(DateTime.Now.Subtract(datetime).Ticks).Year - 1;
        }
    }
}
