﻿using System.Collections.Generic;

namespace ClassesAndInterfaces.Task3
{
    public interface IDataProvider<T>
    {
        void Add(T[] items);

        IEnumerable<T> GetAll();
    }
}
