﻿using ClassesAndInterfaces.Task1;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ClassesAndInterfaces.Task3
{
    public class DataProvider<T> : IDataProvider<T>
    {
        private static readonly ISerializer<T> Serializer;
        private static readonly string FileName;

        static DataProvider()
        {
            Serializer = new OtusXmlSerializer<T>();
            FileName = System.IO.Path.GetTempPath() + @"\" + typeof(T).Name + @"Data.xml";
        }

        public void Add(T[] items)
        {
            var itemStream = Serializer.Serialize(items);

            var uniEncoding = new UTF8Encoding();
            var result = uniEncoding.GetBytes(itemStream);

            using (var fileStream = File.Open(FileName, FileMode.Create))
            {
                fileStream.Write(result, 0, result.Length);
            }
        }

        public IEnumerable<T> GetAll()
        {
            if (!File.Exists(FileName))
            {
                return Array.Empty<T>();
            }

            OtusStreamReader<T> items;
            using (var fileStream = File.OpenRead(FileName))
            {
                items = new OtusStreamReader<T>(fileStream, Serializer);
            }

            return items;
        }
    }
}
