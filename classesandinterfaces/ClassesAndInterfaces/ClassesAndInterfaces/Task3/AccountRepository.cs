﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ClassesAndInterfaces.Task3
{
    public class AccountRepository : IRepository<Account>
    {
        private readonly DataProvider<Account> dataProvider;

        public AccountRepository()
        {
            dataProvider = new DataProvider<Account>();
        }

        public IEnumerable<Account> GetAll()
        {
            var items = dataProvider.GetAll();

            foreach (var item in items)
            {
                yield return item;
            }
        }

        public Account GetOne(Func<Account, bool> predicate)
        {
            return dataProvider.GetAll().FirstOrDefault(predicate);
        }

        public void Add(Account item)
        {
            var items = GetAll().ToList();
            items.Add(item);

            dataProvider.Add(items.ToArray());
        }
    }
}
