﻿using ClassesAndInterfaces.Task3;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;

namespace ClassesAndInterfaces.Tests
{
    [TestClass]
    public class IAccountServiceTests
    {
        private readonly Account[] accounts =
        {
            new Account { FirstName = "Some", LastName = "Name", BirthDate = DateTime.Parse("08/18/2000") },
            new Account { FirstName = "Other", LastName = "Name",  BirthDate = DateTime.Parse("08/18/1900") },
        };

        private readonly Account accountLess18 = new Account { FirstName = "AA", LastName = "AAA", BirthDate = DateTime.Now.AddYears(-17) };
        private readonly Account accountMore18 = new Account { FirstName = "BB", LastName = "BBB", BirthDate = DateTime.Now.AddYears(-19) };
        private readonly Account accountEquals18 = new Account { FirstName = "CC", LastName = "CCC", BirthDate = DateTime.Now.AddYears(-18) };

        [TestMethod]
        public void Add_AccountAgeMore18_Correct()
        {
            var mock = new Mock<IRepository<Account>>();
            mock.Setup(m => m.GetAll()).Returns(GetAll());

            var target = new AccountService(mock.Object);
            target.AddAccount(accountMore18);

            mock.Verify(x => x.Add(accountMore18), Times.Once);
        }

        [TestMethod]
        public void Add_AccountAgeEqual18_Correct()
        {
            var mock = new Mock<IRepository<Account>>();
            mock.Setup(m => m.GetAll()).Returns(GetAll());

            var target = new AccountService(mock.Object);
            target.AddAccount(accountEquals18);

            mock.Verify(x => x.Add(accountEquals18), Times.Once);
        }

        [TestMethod]
        public void Add_AccountAgeLess18_Incorrect()
        {
            var mock = new Mock<IRepository<Account>>();
            mock.Setup(m => m.GetAll()).Returns(GetAll());

            var target = new AccountService(mock.Object);

            try
            {
                target.AddAccount(accountLess18);
            }
            catch (Exception e)
            {
            }

            mock.Verify(x => x.Add(accountLess18), Times.Never);
        }

        private IEnumerable<Account> GetAll()
        {
            return accounts;
        }
    }
}
