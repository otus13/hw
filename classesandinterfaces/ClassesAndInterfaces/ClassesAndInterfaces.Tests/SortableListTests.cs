﻿using ClassesAndInterfaces.Task1;
using ClassesAndInterfaces.Task2;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ClassesAndInterfaces.Tests
{

    [TestClass]
    public class SortableListTests
    {

        private readonly SortableList<Person> persons = new SortableList<Person>
        {
            new Person { FirstName = "AAA", LastName = "A", Age = 21},
            new Person { FirstName = "BBB", LastName = "B", Age = 56},
            new Person { FirstName = "CCC", LastName = "C", Age = 45},
            new Person { FirstName = "DDD", LastName = "D", Age = 18},
        };
        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void Sort_personlist_sorted()
        {
            persons.Sort();

            for (var i = 0; i < persons.Count - 1; i++)
            {
                Assert.IsFalse(persons[i].Age > persons[i + 1].Age);
            }
        }
    }
}
