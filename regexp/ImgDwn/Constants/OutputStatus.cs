﻿namespace ImgDwn.Constants
{
    public static class OutputStatus
    {
        public const string NotWellFormedUri = "not well formed Uri absolute path";
        public const string DownloadSuccesful = "download successful";
        public const string FileAlreadyExists = "file already exists";
    }
}
