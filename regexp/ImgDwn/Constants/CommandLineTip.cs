﻿namespace ImgDwn.Constants
{
    public static class CommandLineTip
    {
        public const string UrlDescription = @"Uri to the web page";
        public const string DestinationPathDescription = @"Download images to the specified folder -d:\files";
    }
}
