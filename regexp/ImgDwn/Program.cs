﻿using ImgDwn.Constants;
using ImgDwn.RegexLib;
using McMaster.Extensions.CommandLineUtils;
using System;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Text;

namespace ImgDwn
{
    class Program
    {
        [Argument(0, Description = CommandLineTip.UrlDescription)]
        [Required]
        public string Url { get; }

        [Option(Description = CommandLineTip.DestinationPathDescription)]
        public string DestinationPath { get; }

        public static int Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;

            return CommandLineApplication.Execute<Program>(args);
        }

        private void OnExecute()
        {
            using var webClient = new WebClient();

            var htmlText = webClient.DownloadString(Url);

            var htmlImages = HtmlParser.GetHtmlImages(htmlText);

            foreach (var htmlImage in htmlImages)
            {
                if (DestinationPath != null)
                {
                    htmlImage.Download(webClient.DownloadFile, DestinationPath);
                }

                Console.WriteLine(htmlImage.ToString());
            }
        }
    }
}
