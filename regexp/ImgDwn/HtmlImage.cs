﻿using ImgDwn.Constants;
using System;
using System.IO;

namespace ImgDwn
{
    public class HtmlImage
    {
        private readonly string uri;
        private string OperationStatus { get; set; }

        public HtmlImage(string uri) => this.uri = uri;

        public void Download(Action<string, string> download, string path)
        {
            try
            {
                if (!Uri.IsWellFormedUriString(uri, UriKind.Absolute))
                {
                    throw new Exception(OutputStatus.NotWellFormedUri);
                }

                var imageUri = new Uri(uri);

                var destinationPath = Path.Combine(path, Path.GetRelativePath(Path.GetPathRoot(path), imageUri.AbsolutePath));

                if (File.Exists(destinationPath))
                {
                    throw new IOException(OutputStatus.FileAlreadyExists);
                }

                Directory.CreateDirectory(Path.GetDirectoryName(destinationPath));

                download(uri, destinationPath);

                OperationStatus = OutputStatus.DownloadSuccesful;
            }
            catch (Exception exception)
            {
                OperationStatus = exception.Message;
            }
        }

        public override string ToString()
        {
            return $"{uri}    {OperationStatus}";
        }
    }
}
