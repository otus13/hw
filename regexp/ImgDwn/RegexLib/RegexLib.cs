﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace ImgDwn.RegexLib
{
    public class HtmlParser
    {
        public static List<HtmlImage> GetHtmlImages(string htmlText)
        {
            var imgSrcRegexp = new Regex(@$"img.+?src ?=[ '""]?(?<url>.+?)[""' >]", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            var uriMatches = imgSrcRegexp.Matches(htmlText);

            var uris = new List<HtmlImage>();
            foreach (Match match in uriMatches)
            {
                uris.Add(new HtmlImage(match.Groups["url"].Value));
            }

            return uris.Distinct().ToList();
        }
    }
}
