﻿using System;
using System.Linq;
using System.Reflection;

namespace serializer
{
    public static class CsvSerializer
    {
        private static BindingFlags Flags => BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public;

        public static string SerializeFields(object obj)
        {
            var fields = obj.GetType().GetFields(Flags);

            var fieldArray = (from f in fields
                              select $"{f.Name},{f.GetValue(obj).ToString()}");

            return string.Join("\n", fieldArray);
        }

        public static void DeserializeFields(object obj, string csv)
        {
            var values = csv.Split('\n');

            foreach (var value in values)
            {
                var arr = value.Split(',');

                var field = obj.GetType().GetField(arr[0], Flags);

                if (field != null)
                {
                    field.SetValue(obj, Convert.ChangeType(arr[1], field.FieldType));
                }
            }
        }
    }
}
