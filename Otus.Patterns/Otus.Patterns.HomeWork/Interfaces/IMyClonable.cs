﻿namespace Otus.Patterns.HomeWork.Interfaces
{
    interface IMyCloneable<out T>
        where T : class
    {
        T Copy();
    }
}
