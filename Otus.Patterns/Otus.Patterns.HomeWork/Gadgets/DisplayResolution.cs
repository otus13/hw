﻿using System;
using Otus.Patterns.HomeWork.Interfaces;

namespace Otus.Patterns.HomeWork.Gadgets
{
    public class DisplayResolution : IMyCloneable<DisplayResolution>, ICloneable
    {
        public int Width { get; set; }
        public int Height { get; set; }

        public DisplayResolution Copy()
        {
            return new DisplayResolution
            {
                Width = this.Width,
                Height = this.Height
            };
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
