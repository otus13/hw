﻿using System;
using Otus.Patterns.HomeWork.Interfaces;

namespace Otus.Patterns.HomeWork.Gadgets
{
    public class Gadget : IMyCloneable<Gadget>, ICloneable
    {
        public string Vendor { get; set; }

        protected Gadget()
        {
        }

        protected Gadget(string vendor)
        {
            Vendor = vendor;
        }

        public virtual Gadget Copy()
        {
            return new Gadget(Vendor);
        }

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        public override bool Equals(object? obj)
        {
            if (ReferenceEquals(this, obj)) return true;
            if (ReferenceEquals(obj, null)) return false;
            if (this.GetType() != obj.GetType()) return false;
            return this.Vendor == ((Gadget)obj).Vendor;
        }
    }
}
