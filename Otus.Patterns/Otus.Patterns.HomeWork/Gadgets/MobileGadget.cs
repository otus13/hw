﻿using Otus.Patterns.HomeWork.Interfaces;
using System;

namespace Otus.Patterns.HomeWork.Gadgets
{
    public class MobileGadget : Gadget, IMyCloneable<MobileGadget>, ICloneable
    {
        public int LifeTimeHours { get; set; }
        public bool HasNfc { get; set; }
        public DisplayResolution DisplayResolution { get; set; }

        public MobileGadget()
        {
        }

        public MobileGadget(
            string vendor, int lifeTimeHours, bool hasNfc, DisplayResolution displayResolution)
            : base(vendor)
        {
            LifeTimeHours = lifeTimeHours;
            HasNfc = hasNfc;
            DisplayResolution = displayResolution;
        }

        public override MobileGadget Copy()
        {
            return new MobileGadget(Vendor, LifeTimeHours, HasNfc, DisplayResolution);
        }

        public override object Clone()
        {
            var gadget = (MobileGadget)base.Clone();
            gadget.DisplayResolution = (DisplayResolution)DisplayResolution.Clone();
            return gadget;
        }

        public override bool Equals(object? obj)
        {
            if (!base.Equals(obj)) return false;

            return LifeTimeHours == (obj as MobileGadget)?.LifeTimeHours &&
                   HasNfc == ((MobileGadget)obj).HasNfc &&
                   DisplayResolution.Height == (((MobileGadget)obj).DisplayResolution.Height) &&
                   DisplayResolution.Width == (((MobileGadget)obj).DisplayResolution.Width);
        }


    }
}
