﻿using System;
using Otus.Patterns.HomeWork.Interfaces;

namespace Otus.Patterns.HomeWork.Gadgets
{
    public class SmartPhone : MobileGadget, IMyCloneable<SmartPhone>, ICloneable
    {
        public int NumberOfSim { get; set; }

        public SmartPhone()
        {
        }

        public SmartPhone(
            string vendor, int lifeTimeHours, bool hasNfc, DisplayResolution displayResolution, int numberOfSim) 
            : base(vendor, lifeTimeHours, hasNfc, displayResolution)
        {
            NumberOfSim = numberOfSim;
        }

        public override SmartPhone Copy()
        {
            return new SmartPhone(Vendor, LifeTimeHours, HasNfc, DisplayResolution, NumberOfSim);
        }

        public override object Clone()
        {
            return (SmartPhone)base.Copy();
        }

        public override bool Equals(object? obj)
        {
            if (!base.Equals(obj)) return false;

            return NumberOfSim == (obj as SmartPhone)?.NumberOfSim;
        }
    }
}
