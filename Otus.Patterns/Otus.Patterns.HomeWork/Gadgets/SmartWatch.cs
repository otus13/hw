﻿using System;
using System.Drawing;
using Otus.Patterns.HomeWork.Interfaces;

namespace Otus.Patterns.HomeWork.Gadgets
{
    public class SmartWatch : MobileGadget, IMyCloneable<SmartWatch>, ICloneable
    {
        public KnownColor BraceletColor { get; set; }
        public bool HasOxygenMeasurement { get; set; }

        public SmartWatch()
        {
        }

        public SmartWatch(
            string vendor, int lifeTimeHours, bool hasNfc, DisplayResolution displayResolution,
            KnownColor braceletColor, bool hasOxygenMeasurement) 
            : base(vendor, lifeTimeHours, hasNfc, displayResolution)
        {
            BraceletColor = braceletColor;
            HasOxygenMeasurement = hasOxygenMeasurement;
        }
        
        public override SmartWatch Copy()
        {
            return new SmartWatch(Vendor, LifeTimeHours, HasNfc, DisplayResolution, BraceletColor,
                HasOxygenMeasurement);
        }

        public override SmartWatch Clone()
        {
            return (SmartWatch)base.Copy();
        }

        public override bool Equals(object? obj)
        {
            if (!base.Equals(obj)) return false;

            return BraceletColor == (obj as SmartWatch)?.BraceletColor &&
                   HasOxygenMeasurement == ((SmartWatch)obj).HasOxygenMeasurement;
        }

    }
}
