using Otus.Patterns.HomeWork.Gadgets;
using Xunit;

namespace Otus.Patterns.HomeWork.Tests
{
    public class SmartPhoneTests
    {
        public SmartPhone SmartPhone => new()
        {
            LifeTimeHours = 100,
            HasNfc = true,
            Vendor = "Apple",
            DisplayResolution = new DisplayResolution { Width = 750, Height = 1334 },
            NumberOfSim = 2
        };

        [Fact]
        public void Clones_Instance_With_Properties()
        {
            var clonedSmartPone = SmartPhone.Copy();

            Assert.True(SmartPhone.Equals(clonedSmartPone));
        }

        [Fact]
        public void Changed_Clone_DoesNot_Affect_Original()
        {
            var clonedSmartPone = SmartPhone.Copy();
            var oldHeight = SmartPhone.DisplayResolution.Height;

            clonedSmartPone.DisplayResolution.Height += 1;

            Assert.Equal(oldHeight, SmartPhone.DisplayResolution.Height);
        }
    }
}
