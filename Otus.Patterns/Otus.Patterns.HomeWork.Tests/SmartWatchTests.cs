using System.Drawing;
using Otus.Patterns.HomeWork.Gadgets;
using Xunit;

namespace Otus.Patterns.HomeWork.Tests
{
    public class SmartWatchTests
    {
        public SmartWatch SmartWatch => new()
        {
            LifeTimeHours = 100,
            HasNfc = true,
            Vendor = "Apple",
            DisplayResolution = new DisplayResolution { Width = 750, Height = 1334 },
            BraceletColor = KnownColor.Red,
            HasOxygenMeasurement = true
        };

        [Fact]
        public void Clones_Instance_With_Properties()
        {
            var clonedSmartWatch = SmartWatch.Copy();

            Assert.True(SmartWatch.Equals(clonedSmartWatch));
        }

        [Fact]
        public void Changed_Clone_DoesNot_Affect_Original()
        {
            var clonedSmartWatch = SmartWatch.Copy();
            var oldHeight = SmartWatch.DisplayResolution.Height;
            
            clonedSmartWatch.DisplayResolution.Height += 1;
            clonedSmartWatch.BraceletColor = KnownColor.ActiveBorder;

            Assert.Equal(oldHeight, SmartWatch.DisplayResolution.Height);
            Assert.Equal(KnownColor.Red, SmartWatch.BraceletColor);
        }
    }
}
